# Application CLI

Création app en CLI => Création et Listing des liens vers les athletes du site https://ufc.com

[![forthebadge](https://forthebadge.com/images/badges/check-it-out.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)


## Modules

```json
"dependencies": {
    "chalk": "^5.0.1",
    "node-emoji": "^1.11.0",
    "validator": "^13.7.0",
    "yargs": "^17.4.1"
  }
```


## Utilisation

```zsh
axellbr@macbook-pro-de-axel cli % node cli.js add --p="athlete" --n="amanda" --ln="nunes"

Lien bien copié!
https://www.ufc.com/athlete/amanda-nunes
axellbr@macbook-pro-de-axel cli % node cli.js all                                        

Liste complète de vos liens :

🔗 https://www.ufc.com/athlete
🔗 https://www.ufc.com/rankings
🔗 https://www.ufc.com/athlete/ciryl-gane
🔗 https://www.ufc.com/athlete/dustin-poirier
🔗 https://www.ufc.com/athlete/amanda-nunes
```

## Licence

Ce projet est sous licence MIT