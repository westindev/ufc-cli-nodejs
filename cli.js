import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import chalk from 'chalk'
import { baseurl } from "./baseURL.js"
import fs, { appendFileSync, readFileSync } from 'fs'
import validator from 'validator'
import emoji from 'node-emoji'

/**
 * yargs
 * * hideBin => Assistant yargs
 * * .command() : Builder command
 * @param1 nom de la commande 
 * @param2 description de la commande 
 * @param3 [objet builder] (construction des options de la commande)
 * @param4 [fonction handler] (fonction gestion de l'argument)
 */
yargs(hideBin(process.argv))
    .command('add', 'ajouter un lien', {
        p: {
            describe: 'page category',
            demandOption: true,
            type: 'string',
        },
        n: {
            describe: 'fighter firstname',
            demandOption: false,
            type: 'string',
        },
        ln: {
            describe: 'figher lastname',
            demandOption: false,
            type: 'string',
        }
    }, (argv) => {
        const { p, n, ln } = argv
        const link = `${baseurl}${p}/${n}-${ln}`;

        if (!validator.isURL(link)) {
            console.log(chalk.red.bold('\nLien incorrect !\n '));
            return;
        }

        appendFileSync("links.txt", `\n${link}`);
        console.log(chalk.inverse.bold('\nLien bien copié!'));
        console.log(chalk.red.bold(link));

    })
    .argv

yargs(hideBin(process.argv))
    .command('all', 'read list of links',
        () => {
        console.log('\nListe complète de vos liens :\n');
        const list = readFileSync('links.txt').toString().split('\n');
        for (let i in list){
            console.log(chalk.green.bold(`${emoji.get('link')} ${list[i]}`));
        }
    }).argv

//console.log(process.argv)  